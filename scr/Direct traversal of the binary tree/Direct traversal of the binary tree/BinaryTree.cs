﻿using System;
using System.Collections.Generic;

namespace Direct_traversal_of_the_binary_tree
{
  public class BinaryTree<TData>
  {
    public enum ConstructType
    {
      Size,
      Levels
    }

    public Node<TData> RootNode => Nodes[0];

    public List<Node<TData>> Nodes { get; private set; }

    public BinaryTree(uint size, ConstructType constructType)
    {
      switch (constructType)
      {
        case ConstructType.Size:
          InitializeBySize(size);
          break;

        case ConstructType.Levels:
          InitializeByLevels(size);
          break;

        default:
          throw new Exception($"Incorrect {nameof(ConstructType)}: \"{constructType}\"!");
      }
    }

    private void InitializeBySize(uint size)
    {
      Nodes = new List<Node<TData>>((int)size) { new Node<TData>() };

      for (int i = 0; i < size; i++)
      {
        var node = Nodes[i];
        CreateChildren(node, size);
      }
    }

    private void InitializeByLevels(uint levels)
    {
      var size = Math.Pow(2, levels) - 1;
      InitializeBySize((uint)size);
    }

    private void CreateChildren(Node<TData> node, uint size)
    {
      while (node.IsCanAddChild && Nodes.Count < size)
      {
        var newNode = new Node<TData>(parent: node);
        node.AddChild(newNode);
        Nodes.Add(newNode);
      }
    }
  }
}