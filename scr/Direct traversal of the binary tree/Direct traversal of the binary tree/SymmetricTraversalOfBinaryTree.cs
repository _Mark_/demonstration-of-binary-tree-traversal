﻿using static System.Console;

namespace Direct_traversal_of_the_binary_tree
{
  public class SymmetricTraversalOfBinaryTree
  {
    public static void PassTree<TData>(BinaryTree<TData> tree)
    {
      Pass(tree.RootNode);
    }

    public static void Pass<TData>(Node<TData> node)
    {
      if (node.Contains(ChildNum.First))
      {
        Pass<TData>(node.GetChild(ChildNum.First));
      }

      WriteLine(node.Data.ToString());

      if (node.Contains(ChildNum.Second))
      {
        Pass<TData>(node.GetChild(ChildNum.Second));
      }
    }
  }
}