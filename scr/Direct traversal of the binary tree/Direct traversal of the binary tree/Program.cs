﻿using static System.Console;

namespace Direct_traversal_of_the_binary_tree
{
  class Program
  {
    static void Main(string[] args)
    {
      BinaryTree<int> tree = new BinaryTree<int>(15, BinaryTree<int>.ConstructType.Size);

      for (int i = 0; i < tree.Nodes.Count; i++)
      {
        var node = tree.Nodes[i];
        node.Data = i;
      }

      foreach (var node in tree.Nodes)
      {
        if (node != null)
          Write($"node: {node.Data}");
        if (node.Parent != null)
          Write($" -> parent: {node.Parent.Data}");
        WriteLine();
      }
      WriteLine();
      WriteLine();
      WriteLine();

      // DirectTraversalBinaryTree.PassTree<int>(tree);
      SymmetricTraversalOfBinaryTree.PassTree(tree);
      Read();
    }
  }
}