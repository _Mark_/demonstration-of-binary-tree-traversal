﻿using System;
using System.Collections.Generic;

namespace Direct_traversal_of_the_binary_tree
{
  public class Node<TData>
  {
    public const int MaxChildrenQuantity = 2;

    public TData Data { get; set; }

    public List<Node<TData>> Children { get; } = new List<Node<TData>>(MaxChildrenQuantity);

    public Node<TData> Parent { get; }

    public Node(Node<TData> parent = null)
    {
      Parent = parent;
    }

    public bool IsHaveChild => Children.Count > 0;

    public bool IsCanAddChild => Children.Count < MaxChildrenQuantity;

    public Node<TData> AddChild(Node<TData> newNode)
    {
      if (Children.Count >= 2)
        throw new IndexOutOfRangeException();

      Children.Add(newNode);
      return this;
    }

    public Node<TData> GetChild(ChildNum childNum)
    {
      switch (childNum)
      {
        case ChildNum.First:
          return Children[0];

        case ChildNum.Second:
          return Children[1];

        default:
          throw new ArgumentOutOfRangeException(nameof(childNum), childNum, null);
      }
    }

    public bool Contains(ChildNum childNum)
    {
      switch (childNum)
      {
        case ChildNum.First:
          if (Children.Count > 0)
          {
            return true;
          }
          break;

        case ChildNum.Second:
          if (Children.Count == MaxChildrenQuantity)
          {
            return true;
          }
          break;

        default:
          throw new ArgumentOutOfRangeException(nameof(childNum), childNum, null);
      }

      return false;
    }

    public bool TryGetChild(ChildNum childNum, out Node<TData> child)
    {
      child = null;
      if (Children.Count == 0) return false;

      switch (childNum)
      {
        case ChildNum.First:
          if (Contains(childNum))
          {
            child = GetChild(childNum);
            return true;
          }
          break;

        case ChildNum.Second:
          if (Contains(childNum))
          {
            child = GetChild(childNum);
            return true;
          }
          break;

        default:
          throw new ArgumentOutOfRangeException(nameof(childNum), childNum, null);
      }

      return false;
    }
  }
}
