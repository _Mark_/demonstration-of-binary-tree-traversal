﻿using static System.Console;

namespace Direct_traversal_of_the_binary_tree
{
  public static class DirectTraversalBinaryTree
  {
    public static void PassTree<TData>(BinaryTree<TData> tree)
    {
      var node = tree.RootNode;
      Pass(node);
    }

    public static void Pass<TData>(Node<TData> node)
    {
      WriteLine(node.Data.ToString());
      if (node.IsHaveChild)
      {
        var child = node.GetChild(ChildNum.First);
        Pass<TData>(child);
      }
      if (node.Contains(ChildNum.Second))
      {
        var child = node.GetChild(ChildNum.Second);
        Pass<TData>(child);
      }
    }
  }
}
